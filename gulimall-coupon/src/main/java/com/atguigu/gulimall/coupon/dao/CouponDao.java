package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author zhangxuelian
 * @email zhangbao4830@gmail.com
 * @date 2021-02-16 15:59:00
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
