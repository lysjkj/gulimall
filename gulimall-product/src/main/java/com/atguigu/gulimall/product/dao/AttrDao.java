package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author chenshun
 * @email zhangbao4830@gmail.com
 * @date 2021-02-02 21:52:24
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
